<?php
class Model {
	private $table = 'db_table';
	private db_instance;

	function __construct {
		$this->db_instance = Db::getInstance(); // It would be faster to operate with local variable a copy of the instance
	}

	public function create($data_arr) {
		// If the request is simple, one can use directly the Db::insert method
		// $this->db_instance->insert

		// In some cases it is bvetter to build the request youreslef
		if(!is_array(data_arr) || count($data_arr) <= 0) {
			return false;
		}

		$sql = "INSERT INTO `"._DB_PREFIX_."`";
		$fields_str = " VALUES (";
		$data_str = " ("
		$update_str = " ON DUPLICATE KEY UPDATE ("
		foreach ($data_arr as $field=>$value) {
			$values_str = $values_str."`".$field."`,";
			$data_str = $data_str.pSql($value).","; // or cast to int if integers or maybe have a field to indicate the data type
			$update_str = $update_str."`".$field."`=".pSql($value).",";
		}
		$fields_str = substr($fields_str,0,-1).")"; // remove the last ,
		$update_str = substr($update_str,0,-1).")";
		$sql = $sql.$values_str.$update_str.$update_str
		return $this->db_instance->execute($sql);
	}

	public function delete($where) {
		
	}
	public function get($fields,where) {

	}
	public function update($data,$where) {
		
	}
}
?>