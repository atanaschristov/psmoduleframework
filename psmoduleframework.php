<?php 
/*
*  @author ATHR <testdevelacc@gmail.com>
*  @copyright  2014-2014 DevTools
*/
if (!defined('_PS_VERSION_'))
	exit;

class PsModuleFramework extends Module {

	/**
	 * Module constructor
	 * @see Module
	 */
	public function __construct()
	{
		$this->name = 'psmodframework';
		$this->tab = 'others';
		$this->version = 0.1;	// Last Modification : 2014-06-20
		$this->author = 'Atansa Christov';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
		// $this->dependencies = array('blockcart'); // If there are module dependenies
		
		parent::__construct();

		$this->displayName = $this->l('Prestashop Module Framework');
        $this->description = $this->l('The Module is used as a base framework for faster module development');
	}

	public function uninstall()
	{
		// If files are copied somewhere , remove them
		// If DB tables are created, drop the,
		// If DB tables are altered, remove changes
		// DO EVERYTHING TO REAMOVE ANY TRACES OF THE MODULE

		return parent::uninstall();
	}


	public function install()
	{
		HTMLMailSender::checkLangs();
		$this->retroCompatibility();

		// Register hooks. There are two types of hooks(action and display hooks) 
		// For every hook there should be a hook method. as the example below
		// For more info about the hooks: http://doc.prestashop.com/display/PS15/Hooks+in+PrestaShop+1.5
		// 
		if (
			parent::install() == false
			|| $this->registerHook('header') == false
			|| $this->registerHook('displayRightColumn') == false
			|| $this->registerHook('displayLeftColumn') == false
			|| $this->registerHook('displayRightColumnProduct') == false
			|| $this->registerHook('actionValidateOrder') == false
		)
			return false;
		return true;
	}

	public function hookActionValidateOrder ($params) {
		// do stuff on ValidateOrder event	
	}
	
	public function hookRightColumn($params)
	{

		$html = '';
		return $html;
	}

	public function hookLeftColumn($params)
	{
		$html = '';
		return $html;
	}

	public function getContent()
	{
		// The configuration page in the backoffice can be prepared and randered here
		
		$html = '';
		return $html;
	}

}

?>
