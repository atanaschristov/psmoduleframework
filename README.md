The module file structure followed the recommended and compatible with prestashop 1.5/1.6 structure: 
- http://doc.prestashop.com/display/PS15/Creating+a+PrestaShop+module
- http://doc.prestashop.com/display/PS16/Creating+a+first+module

./
├── config.xml        - MANDATORY
├── controllers       - OPTIONAL (Should process the requests)
│   ├── admin
│   │   ├── adminModFwController.php
│   │   └── index.php
│   ├── front
│   │   ├── frontModFwController.php - the url to invoke the controller: // index.php?fc=module&module=modframework&controller=test 
│   │   └── index.php
│   └── index.php
├── icon.gif          - MANDATORY
├── icon.png          - MANDATORY
├── index.php         - MANDATORY
├── modframework.php  - MANDATORY
├── override
│   ├── classes
│   │   └── index.php
│   ├── controllers
│   │   └── index.php
│   └── index.php
├── README.md
├── themes
│   ├── index.php
│   └── modules
│       └── index.php
├── translations
│   └── index.php
└── views
    ├── index.php
    └── templates
        ├── admin
        │   └── index.php
        ├── css
        │   └── index.php
        ├── front
        │   └── index.php
        ├── hook
        │   └── index.php
        ├── img
        │   └── index.php
        ├── index.php
        ├── js
        │   └── index.php
        └── pdf
            └── index.php
