/*
	The helper works in combination with <module path>/ajax/ajax.php
 
 	1. The ajax command should be part of the req_url 
 	Ex: <domain>/<module path>/ajax/ajax.php?cmd=store&token=1324q3241ad1212
 	
 	2. Multiple commands can be passed in one request. The commands are devided with |
 	Ex: cmd=store|extract|apply

 	3. The result is returned in an object ret_data, which has the returned values for every command
 	Ex: 
 	ret_data = {
		store: { returned feedback from store command }
		extract: { returned feedback from extract command }
 	}
 	For more detile psmodframework/ajax/ajax.php
 */
function psmod_ajax_helper(reqUrl, dataObj, callback) {
	if (typeof reqUrl !== "undefined" && typeof dataObj === "object") {
		jQuery.ajax({
			url: reqUrl,
			type: 'POST',
			dataType: 'json',
			data: dataObj,
			beforeSend: function () {},
			success: function (retdata, textStatus, xhr) {
				// The ajax should always return a result in roder to verify it is working 
				// The returned data should be object, and since null in js is also an object,
				// we should check for it
				if (retdata == null) {
					console.log("No data returned: " + retdata);
					return;
				}
				switch (typeof retdata) {
				case "object":
					if (retdata.error_code) {
						console.log("Code " + retdata.error_code + " " + retdata.msg);
						alert("Code " + retdata.error_code + " " + retdata.msg);
						return;
					}
					// If a callback is not callable, than return the data as is, 
					// otherwize call the callback by passing the data as an argument
					if (typeof (callback) !== "function") {
						console.log("Callback is not provided.");
						return retdata;
					} else {
						return callback(retdata);
					}
					break;
				default:
					console.log("Wrong returned data type: " + (typeof retdata));
					return;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(textStatus + " - " + errorThrown);
				alert(textStatus + " - " + errorThrown);
				return;
			}
		});
	} else {
		console.log("Lack of arguments");
	}
}