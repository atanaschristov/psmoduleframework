<?php
	class FrontModFwController extends ModuleFrontController {
		public function __construct()
		{
			$allow_guest = true;
			if(!Context::getContext()->customer->isLogged() && (!$allow_guest)) { 
				$this->auth = true; 
				$this->authRedirection = true; 
			}
			parent::__construct();
		}

		public function initContent()
		{
			// index.php?fc=module&module=modframework&controller=test
			parent::initContent();
			$this->process();
			$this->context->smarty->assign('data', "Data in smarty Variable");
			$this->context->smarty->assign('dataArray', array (
				'col1' => "data1",
				'col2' => "data2",
			));

			$this->setTemplate('XXX.tpl');
		}

	} 
?>
