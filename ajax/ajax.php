<?php
include_once(dirname(__FILE__).'/../../../config/config.inc.php');
// include_once(dirname(__FILE__).'/../../../config/settings.inc.php');
// require_once(dirname(__FILE__).'/../../../init.php');
include_once(dirname(__FILE__).'/../api/Api.php');

$data_type = Tools::getValue('data_type');
// make sure we initialize the return data type correctly
if(isset($data_type) && !empty($data_type)) {
	switch ($data_type) {
		case 'json':
		case 'html':
		case 'raw' :
			break;
		default:
			$data_type = 'json';
			break;
	}
} else {
	$data_type = 'json';
}

function sendback_data($data) {
	global $data_type;
	switch ($data_type) {
		case 'raw' :
			// TODO: Do we need to seriallize the arrays and objects?
			if(is_array($data) || is_object($data)) {
				$data = serialize($data);
			}
			break;
		// TODO html
		// case 'html':
		// 	// handle as html string
		// 	$data = htmlentities($data);
		// 	break;
		case 'json':
		default:
			// handle as html string
			$data = json_encode($data);
			break;
				
	}
	return $data;
}


// if(!is_logged) {
// 	echo sendback_data(array('error_code' => -1, 'msg' => 'Error: the user is not logged in'));
// 	return;
// }

// TODO make sure the token it more reliable
// Make sure the request comes from accepted source
$context = Contextcore::getContext();
$remote_token = Tools::getValue('token');
$time_generated = Tools::getValue('time');
$local_token = md5(_COOKIE_KEY_ . $time_generated);
if($local_token != $remote_token) {
 	echo sendback_data(array('error_code' => -1, 'msg' => 'Error: Security tokens do not match'));
 	die();
}

// We allow execution of multiple commands at once
$cmd_str = Tools::getValue('cmd');
$commands = explode('|',$cmd_str);
$feedback_data = array(); // contains the feedback for every command
foreach ($commands as $cmd) {
	$result = null;
	switch($cmd) { // Call the php api method corresponding to every command
		case 'store':
			$input = null; //Tools::getValue('data');
			if(empty($input)) {
				$result = array('error_code' => 1, 'msg'=>'Error: We don\' accept requests with no data');
			} else {
				$result = Api::store($input);
			}
			break;
		case 'extract':
			$input = null; //Tools::getValue('data');
			if(empty($input)) {
				$result = array('error_code' => 1, 'msg'=>'Error: We don\' accept requests with no data');
			} else {
				$result = Api::extract($input);
			}
			break;
		case 'apply':
			$input = null; //Tools::getValue('data');
			if(empty($input)) {
				$result = array('error_code' => 1, 'msg'=>'Error: We don\' accept requests with no data');
			} else {
				$result = Api::apply($input);
			}
			break;
		default:
			$result = array('error_code' => 2, 'msg'=>'Error: Unknown command - '.$cmd);
			break;
	}
	$feedback_data[$cmd] = $result;
}

if(count($feedback_data) <= 0) {
	echo sendback_data(array('error_code' => -1, 'msg' => 'Error: No command provided'));
} else {
	echo sendback_data($feedback_data);
}

?>
